#!/usr/bin/env bash

cp "/home/vagrant/pdc8/Config/bashrc" "/home/vagrant/.bashrc"
cp "/home/vagrant/pdc8/Config/homestead.app" "/etc/nginx/sites-available/homestead.app"
cp "/home/vagrant/pdc8/Config/homestead2.app" "/etc/nginx/sites-available/homestead2.app"
ln -fs "/etc/nginx/sites-available/homestead.app" "/etc/nginx/sites-enabled/homestead.app"
ln -fs "/etc/nginx/sites-available/homestead2.app" "/etc/nginx/sites-enabled/homestead2.app"
cp "/home/vagrant/pdc8/Config/php.ini" "/etc/php5/fpm/php.ini"
service nginx restart
service php5-fpm restart
