cp "./Config/homestead.app" "/etc/nginx/sites-available/homestead.app"
cp "./Config/homestead2.app" "/etc/nginx/sites-available/homestead2.app"

ln -fs "/etc/nginx/sites-available/homestead.app" "/etc/nginx/sites-enabled/homestead.app"
ln -fs "/etc/nginx/sites-available/homestead2.app" "/etc/nginx/sites-enabled/homestead2.app"

sudo service nginx restart
sudo service php5-fpm restart