# Treebook


### Description
Treebook est une application de dématérialisation des panneaux descriptifs des arbres du parc de la Tête d'Or.


### Architecture
Ce projet a été découpé en 4 modules :

* Treebook-Admin : La partie Admin de treebook
* Treebook-Common : Les templates communs aux parties Admin et Front
* Treebook-Front : La partie publique de treebook
* Treebook-Server : L'application côté serveur (API)

### Développement

Ce projet utilise une machine virtuelle (VM) vagrant afin de founir tous les outils nécessaires.
Version de la VM : Laravel Homestead

#### Installation :

* Installer Virtual box
* Installer Vagrant
* Installer la VM : `vagrant box add laravel/homestead`
* Placer le dossier du projet (dépot git) (pdc8) dans le dossier utilisateur.
* Créer les clés SSH dans le dossier ~/.ssh (clé privée : ~/.ssh/id_rsa clé public : ~/.ssh/id_rsa.pub )
* Se rendre à la racine du projet
* Exécuter la commande `vagrant up --provision`
* Se connecter en SSH à la VM : `ssh vagrant@127.0.0.1 -p 2222` (ou `vagrant ssh`)	
* Aller dans le dossier /home/vagrant/pdc8/treebook-server
* Faire `composer global require "laravel/homestead=~2.0"`
* Faire `composer install`
* Se rendre sur [http://192.168.10.10](http://192.168.10.10)

#### Remarques :

* Les fichiers du dépot git sont directement synchronisés avec le serveur (le dossier pdc8 est partagé avec la VM)
* Pour éteindre la VM exécuter (à la racine du projet): `vagrant halt`

#### Pour accéder à la base de donnée de la VM :

* Network type; MySQL (TCP/IP)
* Hostname/IP: `127.0.0.1`
* User: `homestead`
* Password: `secret`
* Port: `33060`