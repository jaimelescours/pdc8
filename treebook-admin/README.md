# Treebook Admin

### Dosssiers

* src : Le code source du projet
* dist : Le site construit via gulp
* gulp : La config Gulp

### Requis

* nodeJS
* Gulp
* Bower

### Commencement

* Cloner le dépôt
* Installer les modules nodeJS (package.json) : `npm install`
* Installer les modules de l'application (bower.json) : `bower install`
* Servir le site en local : `gulp serve`
* [Aller sur le site](http://localhost:3001)
* Développer dans le dossier : src
* Construire le site : `gulp build`
