'use strict';

/**
 * The Sync Controller
 */
angular.module('treebookAdmin')
    .controller('SyncCtrl', function($scope, api) {

        /**
         * Start the sync
         * @return {void}
         */
        $scope.sync = function() {
            // Hide the success message
            $scope.success = false;
            // Start the spin
            $scope.spinSync = true;
            // Request the API
            api.sync.get().then(function() {
                // On success
                // Stop the spin
                $scope.spinSync = false;
                // Display the success message
                $scope.success = true;
            }, function() {
                // On error
                // Stop the spin
                $scope.spinSync = false;
            });
        };
    });
