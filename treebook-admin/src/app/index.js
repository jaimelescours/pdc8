'use strict';

/**
 * Define the treeboofAdmin angular module
 */
angular.module('treebookAdmin', [
    'ngSanitize',
    'restangular',
    'ngRoute',
    'uiGmapgoogle-maps',
    'textAngular',
    'focusOn',
    'dateParser',
    'angularUtils.directives.dirPagination',
    'wu.masonry',
    'ui.bootstrap',
    'oitozero.ngSweetAlert',
    'angularSpinner',
    'angular.filter',
    'uz.mailto',
    'frapontillo.bootstrap-switch',
    'angularFileUpload'
])

// Edit the config
.config(function($routeProvider, $locationProvider, RestangularProvider, $tooltipProvider) {

    // Define the routes
    $routeProvider
        .when('/trees', {
            templateUrl: 'app/main/main.html',
            controller: 'MainCtrl'
        })
        .when('/trees/:treeId', {
            templateUrl: 'app/editor/editor.html',
            controller: 'EditorCtrl'
        })
        .when('/contacts', {
            templateUrl: 'app/contacts/contacts.html',
            controller: 'ContactsCtrl'
        })
        .when('/sync', {
            templateUrl: 'app/sync/sync.html',
            controller: 'SyncCtrl'
        })
        .otherwise({
            redirectTo: '/trees'
        });

    // Remove the '#' of url
    $locationProvider.html5Mode(true);

    // Set the base url of the API
    RestangularProvider.setBaseUrl('/api');

    // Configure tooltips
    $tooltipProvider.options({
        appendToBody: true
    });

});
