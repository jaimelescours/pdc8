'use strict';

/**
 * The Contacts Controller
 */
angular.module('treebookAdmin')
    .controller('ContactsCtrl', function($scope, api, Restangular, $rootScope, $dateParser, Mailto, $filter, SweetAlert, usSpinnerService) {
        // Start the global spinner
        usSpinnerService.spin('spinner-1');

        // Get the contacts from the API
        api.contacts.getList().then(function(contacts) {
            // On success

            // Make the date displayable
            angular.forEach(contacts, function(c) {
                c.dateDisplayable = $dateParser(c.date, 'yyyy-MM-dd H:mm:ss');
            });

            // Save the contacts in the model
            $scope.contacts = contacts;

            // Stop the spin
            usSpinnerService.stop('spinner-1');
        }, function() {
            // On error

            // Stop the spin
            usSpinnerService.stop('spinner-1');
        });

        // The number of messages unread
        $scope.nb = $rootScope.nb;

        // Pagination variables
        $scope.pagination = {
            current: 1
        };
        $scope.pageSize = 15;

        /**
         * Put the message read
         * @param  contact The message
         * @return {void}
         */
        $scope.readContact = function(contact) {
            // Start the spin
            $scope.readSpin = true;

            // Copy the object to update
            var c = Restangular.copy(contact);
            // Put it read
            c.read = true;

            // Request the API
            c.put().then(function() {
                // On success

                // Update the model
                contact.read = 1;
                contact.open = false;

                // Update the number of messages unread
                $scope.nb.unread = $scope.nb.unread - 1;

                // Stop the spin
                $scope.readSpin = false;
            }, function() {
                // On error

                // Stop the spin
                $scope.readSpin = false;
            });
        };

        /**
         * Put the message unread
         * @param  contact The message
         * @return {void}
         */
        $scope.unreadContact = function(contact) {
            // Start the spin
            $scope.unreadSpin = true;

            // Copy the object to update
            var c = Restangular.copy(contact);
            // Put it unread
            c.read = false;

            // Request the API
            c.put().then(function() {
                // On success
                // Update the model
                contact.read = 0;

                // Update the number of messages unread
                $scope.nb.unread = $scope.nb.unread + 1;

                // Stop the spin
                $scope.unreadSpin = false;
            }, function() {
                // On error
                // Stope the spin
                $scope.unreadSpin = false;
            });
        };

        /**
         * Remove the message
         * @param  contact The message to remove
         * @return {void}
         */
        $scope.removeContact = function(contact) {
            // Define the confirmation box
            SweetAlert.swal({
                title: 'Êtes-vous sûr ?',
                text: 'Êtes-vous sûr de vouloir supprimer ce message ?',
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Annuler',
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Oui'
            }, function() {
                // On confirmation
                // Start the spin
                $scope.removeSpin = true;
                // Request the API
                contact.remove().then(function() {
                    // On success
                    // Find and remove the message of the model
                    var index = $scope.contacts.indexOf(contact);
                    $scope.contacts.splice(index, 1);

                    if (!contact.read) {
                        // Update the number of messages unread
                        $scope.nb.unread = $scope.nb.unread - 1;
                    }

                    // Stop the spin
                    $scope.removeSpin = false;
                }, function() {
                    // On error
                    // Remove the spin
                    $scope.removeSpin = false;
                });
            });
        };


        /**
         * Build the mailto address to answer to a message
         * @param  contact      The message to answer
         * @return {String}     The mailto address
         */
        $scope.mailto = function(contact) {
            var recepient = contact.email;
            var options = {
                subject: contact.subject,
                body: '\n\n----------\nVotre message :\n' + contact.message
            };

            return Mailto.url(recepient, options);
        };

        /**
         * The personnalized filter to show only the unread messages
         *
         * @param  contact          The message to determine if it should be displayed
         * @return {Boolean}        True:   The message must be displayed
         *                          False:  The message may not be displayed
         */
        $scope.filter = function(contact) {
            // If we show only unread messages
            if ($scope.onlyUnread) {
                return !contact.read;
            } else {
                return true;
            }
        };

        /**
         * Check if the message is read
         * @param  contact          The contact to check
         * @return {Boolean}        True :  The message is read
         *                          False : The message is unread
         */
        $scope.isRead = function(contact) {
            if (angular.isNumber(contact.read)) {
                return contact.read !== 0;
            } else if (angular.isString(contact.read)) {
                return contact.read !== '0';
            } else {
                return contact.read;
            }

        };
    });
