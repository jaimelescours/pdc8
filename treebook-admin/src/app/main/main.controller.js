'use strict';

/**
 * The Main Controller
 */
angular.module('treebookAdmin')
    .controller('MainCtrl', function($scope, $filter, api, usSpinnerService) {

        // Start the global spinner
        usSpinnerService.spin('spinner-1');

        // Request the API
        api.trees.one('all').getList().then(function(trees) {
            // On success
            // Save the trees in the model
            $scope.response = trees;

            // Display all trees
            $scope.search();

            // Stop the spinner
            usSpinnerService.stop('spinner-1');
        }, function() {
            // On error
            // Stop the spinner
            usSpinnerService.stop('spinner-1');
        });

        // Pagination variables
        $scope.pageSize = 15;
        $scope.pagination = {
            current: 1
        };

        // The property to sort on.
        // Default : name
        $scope.orderProp = 'name';
        // Reverse the sort
        $scope.reverse = false;

        /**
         * Apply the search query
         * @return {void}
         */
        $scope.search = function() {
            // update the model
            $scope.trees = $filter('filter')($scope.response, $scope.query);

            // Go to the first page
            $scope.pagination.current = 1;
        };

        /**
         * Reset the query
         * @return {void}
         */
        $scope.reset = function() {
            // Reset the query
            $scope.query = '';
            // Apply the query
            $scope.search();
        };

        /**
         * Change the sort
         * Linked on table header
         *
         * @param  {String}     prop    The property to sort on
         * @param  {Boolean}    reverse  Reverse the sort
         * @return {void}
         */
        $scope.orderBy = function(prop, reverse) {
            $scope.orderProp = prop;
            $scope.reverse = reverse;
            $scope.pagination.current = 1;
        };

        /**
         * Determine if the table is ordered on this property
         * @param  {String}  prop The property to test
         * @return {Boolean}      True:     The table is sorted with the property 'prop'
         *                        False:    The table is not sorted with the property 'prop'
         */
        $scope.isOrderedBy = function(prop) {
            return prop === $scope.orderProp;
        };
    });
