'use strict';

/**
 * The Editor Controller
 */
angular.module('treebookAdmin')
    .controller('EditorCtrl', function($scope, $filter, api, $routeParams, $dateParser, $modal, $location, SweetAlert, FileUploader, uiGmapGoogleMapApi, $sanitize) {

        // Get the tree from the API
        api.trees.one($routeParams.treeId).customGET('all').then(function(tree) {

            // Save the tree in the scope
            $scope.tree = tree;

            // Update the dates of comments
            angular.forEach(tree.comments, function(comment) {
                comment.dateDisplayable = $dateParser(comment.date, 'yyyy-MM-dd H:mm:ss');
            });

            // Set the visibility of galleries
            angular.forEach(tree.galleries, function(picture) {
                var visible;

                // Make visible attribute as Boolean
                if (angular.isNumber(picture.visible)) {
                    visible = picture.visible !== 0;
                } else if (angular.isString(picture.visible)) {
                    visible = picture.visible !== '0';
                } else {
                    visible = picture.visible;
                }
                picture.notVisible = !visible;
                picture.dateDisplayable = $dateParser(picture.date, 'yyyy-MM-dd H:mm:ss');
            });

            tree.galleries = $filter('orderBy')(tree.galleries, 'dateDisplayable');

            // Calcul the publci url of the tree
            $scope.url = 'http://' + $location.host();
            if ($location.port() === 3001) {
                $scope.url += ':3000';
            }
            $scope.url += '/trees/' + tree.id;

            // Add the comments in the model
            $scope.search();

            if (tree.longitude && tree.latitude) {
                // Map Config
                uiGmapGoogleMapApi.then(function(maps) {
                    $scope.map = {
                        center: {
                            latitude: $scope.tree.latitude,
                            longitude: $scope.tree.longitude
                        },
                        options: {
                            scrollwheel: false,
                            mapTypeId: maps.MapTypeId.HYBRID
                        },
                        zoom: 16
                    };
                });
            }

            // Save the rate in the model
            $scope.rating = $scope.tree.detail.rating;
        });


        /**
         * ---------------------------------------
         *             Caracteristics
         * ---------------------------------------
         */

        /**
         * Check if the tree is visible
         * @return {Boolean}    True :  The tree is visible
         *                      False : The tree is not visible
         */
        $scope.isVisible = function() {
            if (angular.isNumber($scope.tree.visible)) {
                return $scope.tree.visible !== 0;
            } else if (angular.isString($scope.tree.visible)) {
                return $scope.tree.visible !== '0';
            } else {
                return $scope.tree.visible;
            }
        };

        /**
         * Make the tree public
         * @return {void}
         */
        $scope.show = function() {
            // Start the spin
            $scope.spinShow = true;

            // Request the API
            api.tree($scope.tree.id).customPUT({
                visible: 1
            }, 'visible').then(function() {
                // On success
                // Stop the Spin
                $scope.spinShow = false;

                // Update the model
                $scope.tree.visible = 1;
            }, function() {
                // On error
                // Stop the spin
                $scope.spinShow = false;
            });
        };


        /**
         * Make the tree private
         * @return {void}
         */
        $scope.hide = function() {
            // Start the spin
            $scope.spinHide = true;

            // Request the API
            api.tree($scope.tree.id).customPUT({
                visible: 0
            }, 'visible').then(function() {
                // On success
                // Stop the Spin
                $scope.spinHide = false;

                // Update the model
                $scope.tree.visible = 0;
            }, function() {
                // On error
                // Stop the spin
                $scope.spinHide = false;
            });
        };

        /**
         * Check if the tree is remarkable
         * @return {Boolean}    True :  The tree is remarkable
         *                      False : The tree is not remarkable
         */
        $scope.isRemarkable = function() {
            if (angular.isNumber($scope.tree.detail.remarkable)) {
                return $scope.tree.detail.remarkable !== 0;
            } else if (angular.isString($scope.tree.detail.remarkable)) {
                return $scope.tree.detail.remarkable !== '0';
            } else {
                return $scope.tree.detail.remarkable;
            }
        };

        /**
         * Make the tree remarkable
         * @return {void}
         */
        $scope.makeRemarkable = function() {
            // Start the spin
            $scope.spinRemarkable = true;

            // Request the API
            api.tree($scope.tree.id).customPUT({
                remarkable: 1
            }, 'remarkable').then(function() {
                // On success
                // Stop the Spin
                $scope.spinRemarkable = false;

                // Update the model
                $scope.tree.detail.remarkable = 1;
            }, function() {
                // On error
                // Stop the spin
                $scope.spinRemarkable = false;
            });
        };


        /**
         * Make the tree not remarkable
         * @return {void}
         */
        $scope.makeNotRemarkable = function() {
            // Start the spin
            $scope.spinNotRemarkable = true;

            // Request the API
            api.tree($scope.tree.id).customPUT({
                remarkable: 0
            }, 'remarkable').then(function() {
                // On success
                // Stop the Spin
                $scope.spinNotRemarkable = false;

                // Update the model
                $scope.tree.detail.remarkable = 0;
            }, function() {
                // On error
                // Stop the spin
                $scope.spinNotRemarkable = false;
            });
        };


        /**
         * ---------------------------------------
         *             Description
         * ---------------------------------------
         */

        /**
         * The display mode
         *     true: show the editor
         *     false: show the description
         * @type {Boolean}
         */
        $scope.editionMode = false;

        /**
         * Pass into edition mode
         * Linked to the edit button
         * @return {void}
         */
        $scope.edit = function() {
            // Copy the description of the tree
            // Needed for the cancel function
            $scope.newDescription = $scope.tree.detail.description;
            // Change the display mode
            $scope.editionMode = true;
        };

        /**
         * Save the new description
         * Linked to the save button
         * @return {void}
         */
        $scope.save = function() {
            // Shortcut if the user doesn't change the description
            if ($scope.newDescription === $scope.tree.detail.description) {
                // Change the display mode
                $scope.editionMode = false;
                return;
            }

            // Start the spin
            $scope.spinSave = true;

            // Build the object to send to the API
            var detail = {
                description: $sanitize($scope.newDescription),
                rating: $scope.tree.detail.rating
            };

            // Request the API to update the description
            api.treeDetail($scope.tree.id).customPUT(detail).then(function() {
                // On Success

                // Change the display mode
                $scope.editionMode = false;

                // update the description in the model
                $scope.tree.detail.description = $sanitize($scope.newDescription);

                // Stop the spin
                $scope.spinSave = false;
            }, function() {
                // On error
                // Stop the spin
                $scope.spinSave = false;
            });
        };

        /**
         * Cancel the changes
         * Linked to the cancel button
         * @return {void}
         */
        $scope.cancel = function() {
            $scope.editionMode = false;
        };



        /**
         * ---------------------------------------
         *             Comments
         * ---------------------------------------
         */

        // Comments variables
        $scope.pageSize = 10;
        $scope.pagination = {
            current: 1
        };


        /**
         * Remove the comment of the list in the model
         * Linked to the remove button
         * @param  comment : comment to remove
         */
        $scope.removeComment = function(comment) {
            // Find the index to remove it of the array in the model
            var index = $scope.tree.comments.indexOf(comment);
            $scope.tree.comments.splice(index, 1);

            // Update the array displayed
            $scope.search();
        };

        /**
         * Apply the search query
         * @return {void}
         */
        $scope.search = function() {
            // Apply the query filter
            $scope.comments = $filter('filter')($scope.tree.comments, $scope.query);

            // Go to the first page
            $scope.pagination.current = 1;
        };

        /**
         * Reset the query
         * @return {void}
         */
        $scope.reset = function() {
            // Reset the query
            $scope.query = '';
            // Apply the query
            $scope.search();
        };



        /**
         * ---------------------------------------
         *             Gallery
         * ---------------------------------------
         */

        // Gallery variables
        var galleryModal;
        $scope.queryPicture = {};

        /**
         * Display the picture selected
         * Linked to a click on a picture
         * @param  image The image to display
         * @return {void}
         */
        $scope.displayPicture = function(image) {
            // Set all picture inactive
            angular.forEach($scope.tree.galleries, function(picture) {
                picture.active = false;
            });
            // Set the selected image active
            image.active = true;

            // Show the modal
            galleryModal = $modal.open({
                scope: $scope,
                templateUrl: 'components/editor/editor.gallery.modal.tpl.html'
            });
        };

        /**
         * Remove a picture of the gallery
         * Linked to the button remove
         * @return {void}
         */
        $scope.removePicture = function() {
            // Define the confirmation box
            SweetAlert.swal({
                title: 'Êtes-vous sûr ?',
                text: 'Êtes-vous sûr de vouloir supprimer cette image ?',
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Annuler',
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Oui'
            }, function() {
                // On confirmation

                // Start the spin
                $scope.spinRemove = true;

                // Find the picture selected
                var picture = $filter('where')($scope.tree.galleries, {
                    active: true
                })[0];

                // Request the API
                api.gallery(picture.id).remove().then(function() {
                    // On success

                    // Find the index of the picture to remove
                    var index = $scope.tree.galleries.indexOf(picture);

                    // Define the next element to display
                    if ($scope.tree.galleries.length - 1 > 0) {
                        if (index === $scope.tree.galleries.length - 1) {
                            $scope.tree.galleries[index - 1].active = true;
                        } else {
                            $scope.tree.galleries[index + 1].active = true;
                        }
                    } else {
                        galleryModal.close();
                    }

                    // Remove the picture of the model
                    $scope.tree.galleries.splice(index, 1);

                    // Stop the spin
                    $scope.spinRemove = false;
                }, function() {
                    // On error

                    // Stop the spin
                    $scope.spinRemove = false;
                });
            });
        };

        /**
         * Check if the picture displyed in the carousel is visible
         * @return {Boolean}    True :  The picture is visible
         *                      False : The picture is not visible
         */
        $scope.pictureIsVisible = function() {
            // Find the picture selected
            var picture = $filter('where')($scope.tree.galleries, {
                active: true
            })[0];
            return !picture.notVisible;
        };

        /**
         * Make the picture public
         * @return {void}
         */
        $scope.showPicture = function() {
            // Find the picture selected
            var picture = $filter('where')($scope.tree.galleries, {
                active: true
            })[0];

            // Start the spin
            $scope.spinShowPicture = true;

            // Request the API
            api.gallery(picture.id).customPUT({
                visible: 1
            }, 'visible').then(function() {
                // On success
                // Stop the Spin
                $scope.spinShowPicture = false;

                // Update the model
                picture.visible = 1;
                picture.notVisible = false;
            }, function() {
                // On error
                // Stop the spin
                $scope.spinShowPicture = false;
            });
        };


        /**
         * Make the picture private
         * @return {void}
         */
        $scope.hidePicture = function() {
            // Find the picture selected
            var picture = $filter('where')($scope.tree.galleries, {
                active: true
            })[0];

            // Start the spin
            $scope.spinHidePicture = true;

            // Request the API
            api.gallery(picture.id).customPUT({
                visible: 0
            }, 'visible').then(function() {
                // On success
                // Stop the Spin
                $scope.spinHidePicture = false;

                // Update the model
                picture.visible = 0;
                picture.notVisible = true;
            }, function() {
                // On error
                // Stop the spin
                $scope.spinHidePicture = false;
            });
        };

        // Define the picture uploader
        $scope.uploader = new FileUploader({
            url: '/api/trees/' + $routeParams.treeId + '/galleries',
            alias: 'image',
            autoUpload: true,
            removeAfterUpload: true,
            queueLimit: 1
        });

        /**
         * Start the spin when the upload start
         * @return {void}
         */
        $scope.uploader.onBeforeUploadItem = function() {
            $scope.uploadSpin = true;
        };

        /**
         * On success it adds the picture in the gallery
         * @param  {File}       item     The file item
         * @param  {Picture}    response The response returned by the API
         * @return {void}
         */
        $scope.uploader.onSuccessItem = function(item, response) {
            var visible;

            // Make visible attribute as Boolean
            if (angular.isNumber(response.visible)) {
                visible = response.visible !== 0;
            } else if (angular.isString(response.visible)) {
                visible = response.visible !== '0';
            } else {
                visible = response.visible;
            }
            response.notVisible = !visible;
            // Add the picture uploaded (returned by the API) to the gallery
            $scope.tree.galleries.push(response);

            // Stop the spin
            $scope.uploadSpin = false;
        };

    });
