'use strict';

/**
 * The Navbar Controller
 */
angular.module('treebookAdmin')
    .controller('NavbarCtrl', function($scope, api, $rootScope) {
        // Define the model
        $scope.nb = {};
        $rootScope.nb = $scope.nb;
        // On each location change, it update the number of unread messages
        $scope.$on('$routeChangeSuccess', function() {
            // Request the API
            api.contacts.customGET('nbUnread').then(function(response) {
                // On success
                // Update the model
                $rootScope.nb.unread = parseInt(response);
            });
        });

        $scope.navbarCollapsed = true;
    });
