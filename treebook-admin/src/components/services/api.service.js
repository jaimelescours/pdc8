'use strict';

/**
 * The pai service
 */
angular.module('treebookAdmin')
    .service('api', function(Restangular) {

        // The trees base
        this.trees = Restangular.all('trees');

        // The Tree base
        this.treeDetail = function(treeId) {
            return Restangular.one('trees', treeId).one('details');
        };

        // The comment base
        this.tree = function(treeId) {
            return Restangular.one('trees', treeId);
        };

        // The comment base
        this.comment = function(commentId) {
            return Restangular.one('comments', commentId);
        };

        // The gallery base
        this.gallery = function(pictureId) {
            return Restangular.one('galleries', pictureId);
        };

        // The sync base
        this.sync = Restangular.one('synchronizeTrees');

        // The contacts base
        this.contacts = Restangular.all('contacts');
    });
