'use strict';

/**
 * The Comment Controller
 */
angular.module('treebookAdmin')
    .controller('CommentCtrl', function($scope, focus, api, SweetAlert) {

        /**
         * The display mode
         *     true: show the editor
         *     false: show the comment
         * @type {Boolean}
         */
        $scope.editionMode = false;

        /**
         * Pass into edition mode
         * Linked to the edit button
         * @return {void}
         */
        $scope.edit = function() {
            // Copy the message of the comment
            // Needed for the cancel function
            $scope.newMessage = $scope.comment.message;
            // Change the display mode
            $scope.editionMode = true;
            // Put the focus on the editor field
            focus($scope.comment.id);
        };

        /**
         * Save the new comment
         * Linked to the save button
         * @return {void}
         */
        $scope.save = function() {
            // Shortcut if the user doesn't change the message
            if ($scope.newMessage === $scope.comment.message) {
                // Change the display mode
                $scope.editionMode = false;
                return;
            }

            // Start the spin
            $scope.spinSave = true;
            // Build the object to send to the API
            var comment = {
                message: $scope.newMessage,
                pseudo: $scope.comment.pseudo
            };
            // Request the API to update the description
            api.comment($scope.comment.id).customPUT(comment).then(function() {
                // On Success

                // Change the display mode
                $scope.editionMode = false;

                // update the model
                $scope.comment.message = $scope.newMessage;
                // Stop the spin
                $scope.spinSave = false;
            }, function() {
                // On error
                // Stop the spin
                $scope.spinSave = false;
            });
        };

        /**
         * Cancel the changes
         * Linked to the cancel button
         * @return {void}
         */
        $scope.cancel = function() {
            $scope.editionMode = false;
        };

        /**
         * Remove the comment
         * Linked to the remove button
         * @param  comment : comment to remove
         */
        $scope.removeComment = function() {
            // Defined the confirmation dialog
            SweetAlert.swal({
                title: 'Êtes-vous sûr ?',
                text: 'Êtes-vous sûr de vouloir supprimer ce commentaire ?',
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Annuler',
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Oui'
            }, function() {
                // On confirmation

                // Start the spin
                $scope.spinRemove = true;

                // Request the API
                api.comment($scope.comment.id).remove().then(function() {
                    // On success
                    // Stop the Spin
                    $scope.spinRemove = false;

                    // Call the editorCtrl.removeComment to remove the comment of the model
                    $scope.remove($scope.comment);
                }, function() {
                    // On error
                    // Stop the spin
                    $scope.spinRemove = false;
                });
            });
        };


        /**
         * Check if the comment is visible
         * @return {Boolean}        True :  The comment is visible
         *                          False : The comment is not visible
         */
        $scope.isVisible = function() {
            if (angular.isNumber($scope.comment.visible)) {
                return $scope.comment.visible !== 0;
            } else if (angular.isString($scope.comment.visible)) {
                return $scope.comment.visible !== '0';
            } else {
                return $scope.comment.visible;
            }
        };

        /**
         * Make public the comment
         * @return {void}
         */
        $scope.show = function() {
            var comment = $scope.comment;

            // Start the spin
            $scope.spinShow = true;

            // Request the API
            api.comment($scope.comment.id).customPUT({
                visible: 1
            }, 'visible').then(function() {
                // On success
                // Stop the Spin
                $scope.spinShow = false;

                // Update the model
                comment.visible = 1;
            }, function() {
                // On error
                // Stop the spin
                $scope.spinShow = false;
            });
        };


        /**
         * Make private the comment
         * @return {void}
         */
        $scope.hide = function() {
            var comment = $scope.comment;

            // Start the spin
            $scope.spinHide = true;

            // Request the API
            api.comment($scope.comment.id).customPUT({
                visible: 0
            }, 'visible').then(function() {
                // On success
                // Stop the Spin
                $scope.spinHide = false;

                // Update the model
                comment.visible = 0;
            }, function() {
                // On error
                // Stop the spin
                $scope.spinHide = false;
            });
        };

    });
