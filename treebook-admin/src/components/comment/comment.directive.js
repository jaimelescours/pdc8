'use strict';

/**
 * The Comment Directive
 */
angular.module('treebookAdmin')
    .directive('tbComment', function() {
        return {
            restrict: 'A',
            scope: {
                comment: '=',
                remove: '=',
                index: '='
            },
            controller: 'CommentCtrl',
            templateUrl: 'components/comment/comment.html'
        };
    });
