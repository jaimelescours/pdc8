'use strict';

/**
 * The filestyle directive to user the jQuery module
 */
angular.module('treebookAdmin')
    .directive('filestyle', function() {
        return {
            restrict: 'C',
            link: function(scope, element, attrs) {
                var options = {
                    'input': attrs.input === 'false' ? false : true,
                    'icon': attrs.icon === 'false' ? false : true,
                    'buttonBefore': attrs.buttonBefore === 'true' ? true : false,
                    'disabled': attrs.disabled === 'true' ? true : false,
                    'size': attrs.size,
                    'buttonText': attrs.buttonText,
                    'buttonName': attrs.buttonName,
                    'iconName': attrs.iconName,
                    'badge': attrs.badge === 'false' ? false : true
                };

                element.filestyle(options);
            }
        };
    });
