server {
    listen 8080;
    server_name homestead.app;
    root /home/vagrant/pdc8/treebook-admin/dist;
    index index.html;
    charset utf-8;

    access_log off;
    error_log  /var/log/nginx/homestead.app-error.log error;

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }
    location ~ \.(woff|ttf) { access_log off; log_not_found off; }

    location / {
        try_files $uri $uri/ /index.html; 
    }

    location /api/img {
        autoindex off;
        alias /home/vagrant/pdc8/treebook-server/public/img;
    }

    # Make index.php in /api url unnecessary
    location /api {
        root /home/vagrant/pdc8/treebook-server/public;
        try_files $uri /api/index.php$is_args$args;

        client_max_body_size 10M;
    }

    # Only parse PHP for /api/index.php
    location ~ \/api\/index\.php$ {
        root /home/vagrant/pdc8/treebook-server/public;

        client_max_body_size 10M;
        
        fastcgi_split_path_info ^(.+\.php)(/.+)$;

        fastcgi_pass unix:/var/run/php5-fpm.sock;
        fastcgi_index index.php;

        include fastcgi_params;

        # This specifically sets so the /api/ portion
        # of the URL is not included
        fastcgi_param SCRIPT_FILENAME $document_root/index.php;
        fastcgi_param PATH_INFO       $fastcgi_path_info;
        fastcgi_param ENV production;

    }
    
    location ~ /\.ht {
        deny all;
    }

    sendfile off;    

}