'use strict';

var gulp = require('gulp');

var gutil = require('gulp-util');
var minifyCSS = require('gulp-minify-css');

var $ = require('gulp-load-plugins')({
    pattern: ['gulp-*', 'main-bower-files', 'uglify-save-license', 'del']
});

gulp.task('injector:css', ['wiredep'], function() {
    return gulp.src('src/index.html')
        .pipe($.inject(gulp.src([
            'src/{app,components}/**/*.css'
        ], {
            read: false
        }), {
            ignorePath: 'src',
            addRootSlash: false
        }))
        .pipe(gulp.dest('src/'));
});

gulp.task('jshint', function() {
    return gulp.src('src/{app,components}/**/*.js')
        .pipe($.jshint())
        .on('error', gutil.log)
        .pipe($.jshint.reporter('jshint-stylish'))
        .on('error', gutil.log);
});

gulp.task('injector:js', ['jshint', 'injector:css'], function() {
    return gulp.src('src/index.html')
        .pipe($.inject(gulp.src([
            'src/{app,components}/**/*.js',
            '!src/{app,components}/**/*.spec.js',
            '!src/{app,components}/**/*.mock.js'
        ]).pipe($.angularFilesort()), {
            ignorePath: 'src',
            addRootSlash: false
        }))
        .pipe(gulp.dest('src/'));
});

gulp.task('partials', function() {
    return gulp.src([
            'src/{app,components}/**/*.html',
            '../treebook-common/**/*.html'
        ])
        .pipe($.minifyHtml({
            empty: true,
            spare: true,
            quotes: true
        }))
        .pipe($.angularTemplatecache('templateCacheHtml.js', {
            module: 'treebookFront'
        }))
        .pipe(gulp.dest('.tmp/inject/'));
});

gulp.task('html', ['wiredep', 'injector:css', 'injector:js', 'partials'], function() {
    var htmlFilter = $.filter('*.html');
    var jsFilter = $.filter('**/*.js');
    var cssFilter = $.filter('**/*.css');
    var assets;

    return gulp.src([
            'src/*.html'
        ])
        .pipe($.inject(gulp.src('.tmp/inject/templateCacheHtml.js', {
            read: false
        }), {
            starttag: '<!-- inject:partials -->',
            ignorePath: '.tmp',
            addRootSlash: false
        }))
        .pipe(assets = $.useref.assets())
        .pipe($.rev())
        .pipe(jsFilter)
        .pipe($.ngAnnotate())
        .pipe($.uglify({
            preserveComments: $.uglifySaveLicense
        }))
        .pipe(jsFilter.restore())
        .pipe(cssFilter)
        .pipe(minifyCSS())
        .pipe(cssFilter.restore())
        .pipe(assets.restore())
        .pipe($.useref())
        .pipe($.revReplace())
        .pipe(htmlFilter)
        .pipe($.minifyHtml({
            empty: true,
            spare: true,
            quotes: true
        }))
        .pipe(htmlFilter.restore())
        .pipe(gulp.dest('dist/'))
        .pipe($.size({
            title: 'dist/',
            showFiles: true
        }));
});

gulp.task('images', function() {
    return gulp.src('src/assets/images/**/*')
        .pipe($.imagemin({
            optimizationLevel: 3,
            progressive: true,
            interlaced: true
        }))
        .pipe(gulp.dest('dist/assets/images/'));
});

gulp.task('fonts', function() {
    var mainBowerFiles = $.mainBowerFiles();
    mainBowerFiles.push('/home/vagrant/pdc8/treebook-front/bower_components/bootstrap/dist/fonts/glyphicons-halflings-regular.woff2');
    return gulp.src(mainBowerFiles)
        .pipe($.filter('**/*.{eot,svg,ttf,woff,woff2}'))
        .pipe($.flatten())
        .pipe(gulp.dest('dist/fonts/'));
});

gulp.task('misc', function() {
    return gulp.src('src/**/*.ico')
        .pipe(gulp.dest('dist/'));
});

gulp.task('clean', function(done) {
    $.del(['dist/', '.tmp/'], done);
});

gulp.task('build', ['clean', 'html', 'images', 'fonts', 'misc']);
