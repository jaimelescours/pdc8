'use strict';

/**
 * The File style directive
 * This is to display the file input button with bootstrap style
 */
angular.module('treebookFront')
    .directive('filestyle', function() {
        return {
            restrict: 'C',
            link: function(scope, element, attrs) {
                var options = {
                    'input': attrs.input === 'false' ? false : true,
                    'icon': attrs.icon === 'false' ? false : true,
                    'buttonBefore': attrs.buttonBefore === 'true' ? true : false,
                    'disabled': attrs.disabled === 'true' ? true : false,
                    'size': attrs.size,
                    'buttonText': attrs.buttonText,
                    'buttonName': attrs.buttonName,
                    'iconName': attrs.iconName,
                    'badge': attrs.badge === 'false' ? false : true
                };

                element.filestyle(options);
            }
        };
    });
