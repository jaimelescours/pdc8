'use strict';

angular.module('treebookFront')
    .directive('tbComment', function() {
        return {
            restrict: 'A',
            scope: {
                comment: '=',
                remove: '=',
                index: '='
            },
            controller: 'CommentCtrl',
            templateUrl: 'components/comment/comment.html'
        };
    });
