'use strict';

angular.module('treebookFront')
    .directive('navbarNav', function($location) {
        var defaults = {
            activeClass: 'active',
            routeAttr: 'data-match-route',
            strict: false
        };

        return {
            restrict: 'C',
            link: function(scope, element, attr) {
                // Directive options
                var options = angular.copy(defaults);
                angular.forEach(Object.keys(defaults), function(key) {
                    if (angular.isDefined(attr[key])) {
                        options[key] = attr[key];
                    }
                });

                // Watch for the $location
                scope.$watch(function() {

                    return $location.path();

                }, function(newValue) {

                    var liElements = element[0].querySelectorAll('li[' + options.routeAttr + ']');

                    angular.forEach(liElements, function(li) {

                        var liElement = angular.element(li);
                        var pattern = liElement.attr(options.routeAttr).replace('/', '\\/');
                        if (options.strict) {
                            pattern = '^' + pattern + '$';
                        }
                        var regexp = new RegExp(pattern, ['i']);

                        if (regexp.test(newValue)) {
                            liElement.addClass(options.activeClass);
                        } else {
                            liElement.removeClass(options.activeClass);
                        }

                    });

                });
            }
        };
    });
