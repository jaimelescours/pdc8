'use strict';

angular.module('treebookFront')
    .controller('NavbarCtrl', function($scope) {
        $scope.navbarCollapsed = true;
    });
