'use strict';

/**
 * The onlyRemarkable filter
 * This return the remarkable tree if the onlyRemarkable attribut is true
 */
angular.module('treebookFront')
    .filter('onlyRemarkable', function() {
        return function(trees, onlyRemarkable) {
            // Define the function to check if a tree is remarkable
            function isRemarkable(tree) {
                return tree.remarkable !== 0 && tree.remarkable !== '0';
            }
            var res;
            if (onlyRemarkable) {
                res = [];

                angular.forEach(trees, function(tree) {
                    if (isRemarkable(tree)) {
                        res.push(tree);
                    }
                });
            } else {
                res = trees;
            }
            return res;
        };
    });
