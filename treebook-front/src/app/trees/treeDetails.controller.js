'use strict';

/**
 * The tree details Controller
 */
angular.module('treebookFront')
    .controller('TreeDetailsCtrl', function($scope, $filter, $routeParams, Restangular, $modal, $dateParser, $timeout, FileUploader, uiGmapGoogleMapApi, $cookies) {

        // get the tree id from the route
        var treeId = $routeParams.treeId;

        //get the tree from db 
        Restangular.one('trees/' + treeId).get().then(function(tree) {
            // On success
            // save  the result in the model
            $scope.tree = tree;

            // Define the map
            if (tree.longitude && tree.latitude) {
                uiGmapGoogleMapApi.then(function(maps) {
                    $scope.map = {
                        center: {
                            latitude: $scope.tree.latitude,
                            longitude: $scope.tree.longitude
                        },
                        options: {
                            scrollwheel: false,
                            mapTypeId: maps.MapTypeId.HYBRID
                        },
                        zoom: 16
                    };
                });
            }

            // Save the comments in the scope
            $scope.comments = $scope.tree.comments;

            // Make dates displayable
            angular.forEach($scope.tree.comments, function(comment) {
                comment.dateDisplayable = $dateParser(comment.date, 'yyyy-MM-dd H:mm:ss');
            });
            angular.forEach($scope.tree.galleries, function(gallery) {
                gallery.dateDisplayable = $dateParser(gallery.date, 'yyyy-MM-dd H:mm:ss');
            });

            // Fix the order of the pictures 
            tree.galleries = $filter('orderBy')(tree.galleries, 'dateDisplayable');
        });

        // Defines the pagination variables
        $scope.pageSize = 10;
        $scope.pagination = {
            current: 1
        };


        // Get the vote from cookie
        if ($cookies.voted) {
            $scope.voted = $cookies.voted;
            $scope.rating = $cookies.rating;
        }

        // Define the vote action
        // Linked to the rating module
        $scope.vote = function(vote) {
            if (!$scope.voted) {
                $scope.error = false;
                var rating = {
                    rating: vote
                };
                //post vote to database
                Restangular.one('trees/' + treeId).customPOST(rating, 'vote').then(function(response) {
                    // On success
                    // Update the cookie
                    $cookies.voted = true;
                    $cookies.rating = vote;
                    // Update the model
                    $scope.voted = true;
                    $scope.tree.detail.rating = response.rating;
                    $scope.tree.detail.nb_rate = response.nb_rate;
                }, function() {
                    // On errur
                    $scope.error = true;
                    $scope.rating = $scope.tree.detail.rating;
                });
            }
        };

        /**
         * ---------------------------------------
         *             Gallery
         * ---------------------------------------
         */

        // Gallery variables
        var galleryModal;

        /**
         * Display the picture selected
         * Linked to a click on a picture
         * @param  image The image to display
         * @return {void}
         */
        $scope.displayPicture = function(image) {
            image.active = true;

            // Show the modal
            galleryModal = $modal.open({
                scope: $scope,
                templateUrl: 'components/gallery/treeDetails.gallery.modal.tpl.html'
            });
        };

        // The file uploader
        $scope.uploader = new FileUploader({
            url: '/api/trees/' + treeId + '/galleries',
            alias: 'image',
            autoUpload: true,
            removeAfterUpload: true,
            queueLimit: 1
        });
        $scope.uploader.onBeforeUploadItem = function() {
            $scope.uploadSpin = true;
        };
        $scope.uploader.onSuccessItem = function(item, response) {
            $scope.tree.galleries.push(response);
            $scope.uploadSpin = false;
        };

        //Signal Button 
        $scope.getPictureId = function() {
            var picture = $filter('where')($scope.tree.galleries, {
                active: true
            })[0];
            return picture.id;
        };

        // Comments
        $scope.newComment = {};
        $scope.commentAlerts = [];
        // Send a new comment to the api
        $scope.send = function(newComment) {
            $scope.sendSpin = true;
            Restangular.one('trees', $scope.tree.id).customPOST(newComment, 'comments').then(function(response) {
                // On success
                // Reset the form
                $scope.commentForm.$setPristine();
                $scope.commentForm.$setUntouched();
                $scope.newComment = {};

                // Make the date of the new comment displayable
                response.dateDisplayable = $dateParser(response.date, 'yyyy-MM-dd H:mm:ss');

                // Display the row of the mesage in green
                response.class = true;
                $timeout(function() {
                    // After 3 seconde it remove the green highlight
                    response.class = false;
                }, 3000);

                // Adds the new comment to the table
                $scope.comments.push(response);

                // Stop the spin
                $scope.sendSpin = false;
            }, function() {
                // On error
                // Display an alert
                $scope.commentAlerts.push({
                    type: 'danger',
                    msg: 'Une erreur est survenue lors de l\'envoi du message, merci de réessayer dans quelques instants.'
                });
                // Stop the spin
                $scope.sendSpin = false;
            });
        };
        // Close en alert
        $scope.closeCommentAlert = function(index) {
            $scope.commentAlerts.splice(index, 1);
        };

    });
