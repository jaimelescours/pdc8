'use strict';

/**
 * The Trees Controller
 */
angular.module('treebookFront')
    .controller('TreesCtrl', function($scope, $location, Restangular, $filter) {

        // Defines the pagination variables
        $scope.pageSize = 10;
        $scope.pagination = {
            current: 1
        };

        // Define the default sort options
        $scope.orderProp = 'name';
        $scope.reverse = false;

        // Request the API
        Restangular.all('trees/remarkable').getList().then(function(trees) {
            // On success
            // Save data in the model
            $scope.response = trees;

            // Display result
            $scope.search();
        });

        // teh search function
        $scope.search = function() {
            // Filter the result
            $scope.trees = $filter('filter')($scope.response, $scope.query);
            // Go to the first page
            $scope.pagination.current = 1;
        };

        // The sort function
        $scope.orderBy = function(prop, reverse) {
            $scope.orderProp = prop;
            $scope.reverse = reverse;
            // go to the first page
            $scope.pagination.current = 1;
        };

        // Check if the table is ordered by this prop
        $scope.isOrderedBy = function(prop) {
            return prop === $scope.orderProp;
        };

        // Reset the search field
        $scope.reset = function() {
            $scope.query = '';
            $scope.search();
        };
    });
