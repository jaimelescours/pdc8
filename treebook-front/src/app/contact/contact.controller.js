'use strict';

/**
 * The Contact Controller
 */
angular.module('treebookFront').controller('ContactCtrl', function($scope, $routeParams, vcRecaptchaService, Restangular) {
    // Define variables
    $scope.contact = {};
    $scope.alerts = [];

    //prefill if signal button provenance
    if ($routeParams.idPhoto && $routeParams.idArbre) {
        $scope.contact.subject = "Signalement de la photo " + $routeParams.idPhoto + " sur l'arbre " + $routeParams.idArbre;
    };

    // Called when the recaptcha is at success
    $scope.onRecaptchaSuccess = function(response) {
        // save the result in the model
        $scope.contact.recaptcha = response;
    };

    // Send action
    // Linked to the send button
    $scope.send = function(valid) {
        // If the form is valid
        if (valid) {
            // Request the API
            Restangular.all('contacts').post($scope.contact).then(function() {
                // On success
                // Reset the  recaptcha
                vcRecaptchaService.reload();
                // Reset the form
                $scope.contactForm.$setPristine();
                $scope.contactForm.$setUntouched();
                $scope.contact = {};

                // Display an alert
                $scope.alerts.push({
                    type: 'success',
                    msg: 'Le message a correctement été transmis, merci.'
                });
            }, function() {
                // On error
                // Reset the recaptcha
                vcRecaptchaService.reload();
                // Display en error alert
                $scope.alerts.push({
                    type: 'danger',
                    msg: 'Une erreur est survenue lors de l\'envoi du message, merci de réessayer dans quelques instants.'
                });
            });
        } else {
            // If the form is not valid
            // Display an error alert
            $scope.alerts.push({
                type: 'danger',
                msg: 'Merci de compléter entièrement le formulaire de contact.'
            });
        }
    };

    // Close an alet
    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };
});
