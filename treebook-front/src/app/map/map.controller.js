'use strict';

/**
 * The Map Controller
 */
angular.module('treebookFront')
    .controller('MapCtrl', function($scope, Restangular, uiGmapGoogleMapApi, $filter, usSpinnerService) {

        // Start the global spinner
        usSpinnerService.spin('spinner-1');

        // Request the API to get trees
        Restangular.all('trees').getList().then(function(trees) {

            // Add control of infoWindow on each tree
            angular.forEach(trees, function(tree) {
                tree.onclicked = function() {
                    tree.showWindow = true;
                };
                tree.closeClick = function() {
                    tree.showWindow = false;
                };
                tree.showWindow = false;
            });

            // Init the map
            uiGmapGoogleMapApi.then(function(maps) {
                $scope.map = {
                    center: {
                        latitude: 45.777403199999990000,
                        longitude: 4.855214400000023000
                    },
                    options: {
                        scrollwheel: false,
                        mapTypeId: maps.MapTypeId.HYBRID
                    },
                    zoom: 14
                };
            });

            // Group all trees by genre_name
            var treesByGenreName = $filter('groupBy')(trees, 'name');

            // Define the array Genres
            // Do it because the ui-select needs an array
            $scope.genres = [];
            angular.forEach(treesByGenreName, function(group, genre) {
                if (genre) {
                    $scope.genres.push({
                        name: genre,
                        group: group
                    });
                }
            });
            $scope.genres = $filter('orderBy')($scope.genres, 'name');

            // Select the first group
            $scope.selection = {
                selected: $scope.genres[0]
            };


            // Stop the spinner
            usSpinnerService.stop('spinner-1');

            // Build the collection of trees to display
            function buildTrees() {
                $scope.trees = $filter('onlyRemarkable')($scope.selection.selected.group, $scope.onlyRemarkable);
            }

            // Define watchers to update the trees collection
            $scope.$watch('onlyRemarkable', buildTrees);
            $scope.$watch('selection', buildTrees, true);

        }, function() {
            // On error
            // Stop the spinner
            usSpinnerService.stop('spinner-1');
        });

    });
