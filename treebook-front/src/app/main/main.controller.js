'use strict';

/**
 * The Main Controller
 */
angular.module('treebookFront')
    .controller('MainCtrl', function(Restangular, $scope, $location, $filter) {

        // Define the pagination variables
        $scope.pageSize = 10;
        $scope.pagination = {
            current: 1
        };

        // Defin the default order
        $scope.orderProp = 'name';
        $scope.reverse = false;

        // Go to the tree page
        $scope.goTo = function(id) {
            $location.url('/trees/' + id);
        };

        // Request the api
        Restangular.all('trees').getList().then(function(trees) {
            // On success
            // Save the result in the model
            $scope.trees = trees;
        });

        // The search function
        // Linked on the search button
        $scope.search = function() {

            // Filter the result on the centralized_id attribut
            $scope.results = $filter('filter')($scope.trees, {
                'centralized_id': $scope.query
            });

            // Shorcuts
            if ($scope.query === '' || $scope.results.length === 0) {
                $scope.results = null;
            }

            // shortcut, go to the tree page if there is one result
            if ($scope.results.length === 1) {
                $scope.goTo($scope.results[0].id);
            }

            // Go to the first page
            $scope.pagination.current = 1;
        };

        // Order the colomn of the table
        $scope.orderBy = function(prop, reverse) {
            $scope.orderProp = prop;
            $scope.reverse = reverse;
            $scope.pagination.current = 1;
        };

        // Check if is ordered by the property prop
        $scope.isOrderedBy = function(prop) {
            return prop === $scope.orderProp;
        };

        // Reset the search field
        $scope.reset = function() {
            $scope.query = '';
            $scope.search();
        };
    });
