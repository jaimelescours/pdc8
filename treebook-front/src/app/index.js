'use strict';

/**
 * Define the treebookFront angular module
 */
angular.module('treebookFront', [
    'ngSanitize',
    'restangular',
    'ngRoute',
    'uiGmapgoogle-maps',
    'angularUtils.directives.dirPagination',
    'wu.masonry',
    'dateParser',
    'ui.bootstrap',
    'vcRecaptcha',
    'monospaced.elastic',
    'angularFileUpload',
    'frapontillo.bootstrap-switch',
    'angular.filter',
    'angularSpinner',
    'ui.select',
    'ngCookies'
])

// Edit the config
.config(function($routeProvider, $locationProvider, RestangularProvider, $tooltipProvider, uiSelectConfig) {

    // Define the routes
    $routeProvider
        .when('/', {
            templateUrl: 'app/main/main.html',
            controller: 'MainCtrl'
        })
        .when('/contact', {
            templateUrl: 'app/contact/contact.html',
            controller: 'ContactCtrl'
        })
        .when('/map', {
            templateUrl: 'app/map/map.html',
            controller: 'MapCtrl'
        })
        .when('/today', {
            templateUrl: 'app/today/today.html',
            controller: 'TodayCtrl'
        })
        .when('/trees', {
            templateUrl: 'app/trees/trees.html',
            controller: 'TreesCtrl'
        })
        .when('/trees/:treeId', {
            templateUrl: 'app/trees/treeDetails.html',
            controller: 'TreeDetailsCtrl'
        })
        .otherwise({
            redirectTo: '/'
        });

    // Remove the '#' of url
    $locationProvider.html5Mode(true);

    // Set the base url of the API
    RestangularProvider.setBaseUrl('/api');

    // Configure tooltips
    $tooltipProvider.options({
        appendToBody: true
    });

    uiSelectConfig.theme = 'bootstrap';
});
