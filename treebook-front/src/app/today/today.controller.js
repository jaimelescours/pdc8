'use strict';

/**
 * The Today Controller
 */
angular.module('treebookFront')
    .controller('TodayCtrl', function(Restangular, $location, usSpinnerService) {
        // Start the global spin
        usSpinnerService.spin('spinner-1');

        //Retrieve today tree from DB
        Restangular.one('today').get().then(function(tree) {

            //Go to today tree page
            $location.url('/trees/' + tree.tree_id);

            // Stop the spin
            usSpinnerService.stop('spinner-1');
        });


    });
