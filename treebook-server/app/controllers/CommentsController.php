<?php

class CommentsController extends BaseController {
	/*
	* Permet d'éditer un commentaire
	*
	* PARAMS : $id, l'attribut "id" du commentaire
	* RETURN : string "successfully edited"
	*/
	public function edit($id) {
		$input = (object)Input::all();
		Comment::where('id', $id)
					->update(array('pseudo' => $input->pseudo, 'message' => $input->message));
		return "successfully edited";
	}

	/*
	* Permet de supprimer un commentaire
	*
	* PARAMS : $id, l'attribut "id" du commentaire
	* RETURN : string "successfully removed"
	*/
	public function delete($id) {
		Comment::where('id', $id)->delete();
		return "successfully removed";
	}

	/*
	* Permet de changer le status d'un commentaire
	*
	* PARAMS : $id, l'attribut "id" du commentaire
	* RETURN : string "successfully updated"
	*/
	public function changeVisible($id) {
		$input = (object)Input::all();
		Comment::where('id', $id)
					->update(array('visible' => $input->visible));
		return "successfully updated";
	}
	
}
