<?php

class ContactsController extends BaseController {

	/*
	* Retourne les messages
	*
	* RETURN : un objet "contact" converti au format JSON
	*/
	public function getMessages() {
		$messages = Contact::all();
		return json_encode($messages);
	}
	
	/*
	* Retourne le nombre de message non lu
	*
	* RETURN : le nombre de message non lu
	*/
	public function getNbUnread() {
		$nb = Contact::where('read', false)->count();
		return $nb;
	}
	
	/*
	* Permet d'ajouter un message
	*
	* RETURN : "successfully added" ou ERROR 500 si la recaptcha n'est pas bon
	*/
	public function addMessage() {
		$recaptcha = json_decode(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=6Lca0gATAAAAAKi1MdPgOfiKwr7HNfNsGIGCZluh&response='.Input::get('recaptcha')), true);
		if($recaptcha['success']){
			$validator = Validator::make(
				array(
					'subject' => Input::get('subject'),
					'message' => Input::get('message'),
					'email' => Input::get('email')
				),
				array(
					'subject' => 'required',
					'message' => 'required',
					'email' => 'required|email'
				)
			);
			if ($validator->fails())
			{
				App::abort(500, $validator->messages());
			} else {
				Contact::insert(
					array('email' => Input::get('email'), 'subject' => Input::get('subject'), 'message' => Input::get('message'), 'read' => 0, 'date' => date("Y-m-d H:i:s"))
				);
				return "successfully added";
			}
		} else {
			App::abort(500, 'Error with recaptcha');
		}
		
	}
	
	/*
	* Permet de changer l'attribut "read" d'un message
	*
	* PARAMS : $id, l'attribut "id" du message
	* RETURN : "successfully edited"
	*/
	public function changeRead($id) {
		Contact::where('id', $id)
					->update(array('read' => Input::get('read')));
		return "successfully edited";
	}
	
	
	/*
	* Permet de supprimer un message
	*
	* PARAMS : $id, l'attribut "id" du message à supprimer
	* RETURN : "successfully removed"
	*/
	public function delete($id) {
		Contact::destroy($id);
		return "successfully removed";
	}

}
