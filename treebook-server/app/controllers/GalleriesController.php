<?php

class GalleriesController extends BaseController {

	/*
	* Permet de modifier le nom d'une image
	*
	* PARAMS : $id, l'attribut "id" de l'image à modifier
	* RETURN : "successfully edited"
	*/
	public function edit($id) {
		$input = (object)Input::all();
		Gallery::where('id', $id)
					->update(array('picture' => $input->picture));
		return "successfully edited";
	}

	/*
	* Permet de supprimer une photo
	*
	* PARAMS : $id, l'attribut "id" de l'image à supprimer
	* RETURN : "successfully removed"
	*/
	public function delete($id) {
		$picture = Gallery::find($id);
		$filename = public_path().substr($picture->picture, 4);
		if (File::exists($filename)) {
		    File::delete($filename);
		}
		$picture->delete();
		return "successfully removed";
	}

	/*
	* Permet de changer le status d'une photo
	*
	* PARAMS : $id, l'attribut "id" de la photo
	* RETURN : string "successfully updated"
	*/
	public function changeVisible($id) {
		$input = (object)Input::all();
		Gallery::where('id', $id)
					->update(array('visible' => $input->visible));
		return "successfully updated";
	}
	
}
