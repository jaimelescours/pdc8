<?php

class TreesController extends BaseController {
	/*
	|--------------------------------------------------------------------------
	| Arbres Générals
	|-------------------------------------------------------------------------- 
	*/
        /*
        * Retourne l'arbre du jour et l'actualise si nécessaire
        *
        * RETURN : JSON : "tree_id" et "date"
        */
        public function getTodayTree() {
                $tree = TodayTree::select(DB::raw('tree_id, date'))
                                        ->where('date', '>', date('Y-m-d 00:00:00'))
                                        ->where('date', '<', date('Y-m-d 23:59:59'))
                                        ->first();
		if(empty($tree)) {
			$treesRemark = Tree::select(DB::raw('trees.id'))
				->join('details', 'details.id', '=', 'trees.detail_id')
				->where('trees.visible', 1)
				->where('details.remarkable', 1)
				->get();
			$random = rand(0, count($treesRemark) - 1);
			TodayTree::insert(array('tree_id' => $treesRemark[$random]->id, 'date' => date("Y-m-d H:i:s")) );
			return json_encode(array("tree_id" => $treesRemark[$random]->id, "date" => date("Y-m-d H:i:s")) );
		} else {
			return json_encode(array("tree_id" => $tree->tree_id, "date" => $tree->date));
		}
        }

	/*
	* Retourne la liste des arbres visibles
	*
	* RETURN : JSON : liste des arbres, contenant tous leurs attributs
	*/
	public function getTrees() {
		$trees = Tree::join('genres', 'genres.id', '=', 'trees.genre_id')
					->join('details', 'details.id', '=', 'trees.detail_id')
					->select(DB::raw('trees.id, trees.name, trees.area, trees.height, trees.trunk_diameter, trees.crown_diameter, trees.longitude, trees.latitude, trees.centralized_id, details.remarkable as remarkable, genres.name as genre_name, genres.species as genre_species, genres.type as genre_type'))
					->where('trees.visible', 1)
					->get();
		return json_encode($trees);
	}
	
	/*
	* Retourne la liste de tous les arbres, visibles ou non
	*
	* RETURN : JSON : liste des arbres, contenant tous leurs attributs
	*/
	public function getAllTrees() {
		$trees = Tree::join('genres', 'genres.id', '=', 'trees.genre_id')
					->join('details', 'details.id', '=', 'trees.detail_id')
					->select(DB::raw('trees.id, trees.name, trees.area, trees.height, trees.trunk_diameter, trees.crown_diameter, trees.longitude, trees.latitude, trees.centralized_id, trees.visible, details.remarkable as remarkable, genres.name as genre_name, genres.species as genre_species, genres.type as genre_type'))
					->get();
		return json_encode($trees);
	}
	
	/*
	|--------------------------------------------------------------------------
	| Arbres Remarquables
	|-------------------------------------------------------------------------- 
	*/
	/*
	* Retourne la liste de tous les arbres remarquables
	*
	* RETURN : JSON : liste des arbres, contenant tous leurs attributs
	*/
	public function getTreesRemarkable() {
		$trees = Tree::join('genres', 'genres.id', '=', 'trees.genre_id')
					->join('details', 'details.id', '=', 'trees.detail_id')
					->select(DB::raw('trees.id, trees.name, trees.area, trees.height, trees.trunk_diameter, trees.crown_diameter, trees.longitude, trees.latitude, trees.centralized_id, genres.name as genre_name, genres.species as genre_species, genres.type as genre_type'))
					->where('trees.visible', 1)
					->where('details.remarkable', 1)
					->get();
		return json_encode($trees);
	}
	
	/*
	* Retourne la liste de tous les arbres remarquables, visibles ou non
	*
	* RETURN : JSON : liste des arbres, contenant tous leurs attributs
	*/
	public function getAllTreesRemarkable() {
		$trees = Tree::join('genres', 'genres.id', '=', 'trees.genre_id')
					->join('details', 'details.id', '=', 'trees.detail_id')
					->select(DB::raw('trees.id, trees.name, trees.area, trees.height, trees.trunk_diameter, trees.crown_diameter, trees.longitude, trees.latitude, trees.centralized_id, trees.visible, genres.name as genre_name, genres.species as genre_species, genres.type as genre_type'))
					->where('details.remarkable', 1)
					->get();
		return json_encode($trees);
	}
	
	/*
	|--------------------------------------------------------------------------
	| Details d'un arbre
	|-------------------------------------------------------------------------- 
	*/
	/*
	* Retourne un arbre identifié par {id} ainsi que ses détails visibles (genre, commentaires, photos, ...)
	*
	* PARAMS : $id, l'attribut "id" de l'arbre
	* RETURN : JSON : arbre avec ses détails (genre/commentaires/...)
	*/
	public function getDetails($id) {
		// ** Tree ************************************************************************************************** //
		$tree = Tree::select(DB::raw('id, name, area, height, trunk_diameter, crown_diameter, longitude, latitude, genre_id, detail_id, centralized_id'))
					->where('id', $id)
					->first();
		// ** Genre ************************************************************************************************** //
		$genre = Genre::select(DB::raw('id, name, species, type'))
					->where('id', $tree->genre_id)
					->first();
		unset($tree['genre_id']);
		$tree['genre'] = $genre;
		// ** Detail ************************************************************************************************** //
		$details = Detail::select(DB::raw('id, description, rating/nb_rate as rating, nb_rate, remarkable'))
					->where('id', $tree->detail_id)
					->first();
		unset($tree['detail_id']);
		$tree['detail'] = $details;
		// ** Comments ************************************************************************************************** //
		$comments = Comment::select(DB::raw('id, pseudo, message, date'))
					->where('tree_id', $tree->id)
					->where('visible', 1)
					->get();
		$tree['comments'] = $comments;
		// ** Galleries ************************************************************************************************** //
		$galleries = Gallery::select(DB::raw('id, picture, date'))
					->where('tree_id', $tree->id)
					->where('visible', 1)
					->get();
		$tree['galleries'] = $galleries;
		return json_encode($tree);
	}
	
	/*
	* Retourne un arbre identifié par {id} ainsi que ses détails, visibles ou non (genre, commentaires, photos, ...)
	*
	* PARAMS : $id, l'attribut "id" de l'arbre
	* RETURN : JSON : arbre avec ses détails (genre/commentaires/...)
	*/
	public function getAllDetails($id) {
		// ** Tree ************************************************************************************************** //
		$tree = Tree::select(DB::raw('id, name, area, height, trunk_diameter, crown_diameter, longitude, latitude, genre_id, detail_id, centralized_id, visible'))
					->where('id', $id)
					->first();
		// ** Genre ************************************************************************************************** //
		$genre = Genre::select(DB::raw('id, name, species, type'))
					->where('id', $tree->genre_id)
					->first();
		unset($tree['genre_id']);
		$tree['genre'] = $genre;
		// ** Detail ************************************************************************************************** //
		$details = Detail::select(DB::raw('id, description, rating/nb_rate as rating, nb_rate, remarkable'))
					->where('id', $tree->detail_id)
					->first();
		unset($tree['detail_id']);
		$tree['detail'] = $details;
		// ** Comments ************************************************************************************************** //
		$comments = Comment::select(DB::raw('id, pseudo, message, date, visible'))
					->where('tree_id', $tree->id)
					->get();
		$tree['comments'] = $comments;
		// ** Galleries ************************************************************************************************** //
		$galleries = Gallery::select(DB::raw('id, picture, date, visible'))
					->where('tree_id', $tree->id)
					->get();
		$tree['galleries'] = $galleries;
		return json_encode($tree);
	}
	
	/*
	* Ajoute un commentaire à un arbre identifié par {id} (par défaut il sera visible)
	*
	* PARAMS : $id, l'attribut "id" de l'arbre
	* RETURN : le commentaire ajouté
	*/
	public function addComment($id) {
		$idComment = Comment::insertGetId(
			array('pseudo' => Input::json('pseudo'), 'message' => Input::json('message'), 'tree_id' => $id, 'date' => date("Y-m-d H:i:s"), 'visible' => 1)
		);
		return Comment::findOrFail($idComment);
	}
	
	/*
	* Ajoute une image à un arbre identifié par {id}
	*
	* PARAMS : $id, l'attribut "id" de l'arbre
	* RETURN : l'image ajoutée ou ERROR 500
	*/
	public function addPicture($id) {
		$input = Input::all();

		$validator = Validator::make($input, array(
        	'image' => 'required|image'
    	));

    	if ($validator->fails())
		{
			App::abort(500, $validator->messages());
		} else {

			$image = Input::file('image');

			$filename = 'image-'.$id.'-'.time().'-'.$image->getClientOriginalName();

			$upload = $image->move
            	(public_path().'/img/', $filename);

        	if ($upload) {
            	$insert_id = Gallery::insertGetId(
					array('picture' => '/api/img/'.$filename, 'tree_id' => $id, 'date' => date("Y-m-d H:i:s"), 'visible' => 1)
				);
				return Gallery::findOrFail($insert_id);
       		}

	        else {
	            App::abort(500, 'Sorry, the image could not be uploaded.');
	        }
		}
	}
	
	/*
	* Ajoute une note à un arbre identifié par {id}
	*
	* PARAMS : $id, l'attribut "id" de l'arbre
	* RETURN : la nouvelle moyenne
	*/
	public function addVote($id) {
		$input = (object)Input::all();
		$tree = Tree::select(DB::raw('detail_id'))
					->where('id', $id)
					->first();
		$detail = Detail::select(DB::raw('rating, nb_rate'))
					->where('id', $tree->detail_id)
					->first();
		if(!is_numeric($input->rating)) {
			return $detail->rating;
		}
		Detail::where('id', $tree->detail_id)
					->update(array('rating' => $detail->rating + $input->rating, 'nb_rate' => ($detail->nb_rate + 1)));

		$detail = Detail::select(DB::raw('rating/nb_rate as rating, nb_rate'))
					->where('id', $tree->detail_id)
					->first();
		return $detail;
	}
	
	/*
	* Modifie les détails d'un arbre identifié par {id}
	*
	* PARAMS : $id, l'attribut "id" de l'arbre
	* RETURN : "successfully edited"
	*/
	public function editDetails($id) {
		$input = (object)Input::all();
		$tree = Tree::select(DB::raw('detail_id'))
					->where('id', $id)
					->first();
		Detail::where('id', $tree->detail_id)
					->update(array('description' => $input->description));
		return "successfully edited";
	}

	/*
	* Permet de changer le status visible d'un arbre
	*
	* PARAMS : $id, l'attribut "id" de l'arbre
	* RETURN : string "successfully updated"
	*/
	public function changeVisible($id) {
		$input = (object)Input::all();
		Tree::where('id', $id)
					->update(array('visible' => $input->visible));
		return "successfully updated";
	}

	/*
	* Permet de changer le status remarquable d'un arbre
	*
	* PARAMS : $id, l'attribut "id" de l'arbre
	* RETURN : string "successfully updated"
	*/
	public function changeRemarkable($id) {
		$input = (object)Input::all();
		print_r($input);
		$tree = Tree::select(DB::raw('detail_id'))
					->where('id', $id)
					->first();
		Detail::where('id', $tree->detail_id)
					->update(array('remarkable' => $input->remarkable));
		return "successfully updated";
	}

}
