<?php ini_set('max_execution_time', 36000); // 10 hours

class SynchronizeController extends BaseController {

	/*
	* Permet de synchroniser les arbres avec la BDD centralisée
	*
	* RETURN : 'BDD synchronized'
	*/
	public function syncTrees()
	{
		$apiGenreId = array();
		$apiTreesId = array();
		$next = "http://rencontres-arbres.herokuapp.com/api/trees/?format=json"; // URL de base de l'API centralisée
		while ($next !== NULL) {
			$json = file_get_contents($next);
			$trees = json_decode($json, true);
			// Si il n'y a pas d'abre suivant, c'est la dernière boucle du while, sinon on stocke l'adresse de l'arbre suivant
			if(!empty($trees['next'])) {
				$next = $trees['next'];
			} else {
				$next = NULL;
			}
			foreach($trees['results'] AS $tree) {
				/*
				 * Genre ******************************************************
				 */
				// On récupère le contenu disponible à l'URL du genre
				$jsonGenre = file_get_contents($tree['genre']);
				$genreJ = json_decode($jsonGenre, true);
				$parts=parse_url($genreJ['url']);
				$path_parts=explode("/", $parts["path"]);
				$apiGenreId[] = $path_parts[3];
				// On récupère le genre ou le crée s'il n'existe pas, puis on le complète avec les infos
				$genreB = Genre::firstOrNew(array('centralized_id' => $path_parts[3]));
				$genreB->name = $genreJ['name'];
				$genreB->species = $genreJ['species'];
				$genreB->type = $genreJ['type'];
				$genreB->save();
				
				/*
				 * Tree ******************************************************
				 */
				$parts=parse_url($tree['url']);
				$path_parts=explode("/", $parts["path"]);
				$apiTreesId[] = $path_parts[3];
				//On récupère l'arbre ou on le crée s'il n'existe pas, puis on le complète avec les infos
				$treeObj = Tree::firstOrNew(array('centralized_id' => $path_parts[3]));
				if(empty($treeObj['name'])) {
					// ** Details ****************************************************** //
					// On crée un Detail pour chaque nouvel arbre
					$detail = new Detail;
					$detail->description = "";
					$detail->rating = 0;
					$detail->save();
					$treeObj->detail_id = $detail->id;
					$treeObj->visible = 1;
				}
				$treeObj->name = $tree['name'];
				$treeObj->area = $tree['area'];
				$treeObj->height = $tree['height'];
				$treeObj->trunk_diameter = $tree['trunk_diameter'];
				$treeObj->crown_diameter = $tree['crown_diameter'];
				$treeObj->longitude = $tree['longitude'];
				$treeObj->latitude = $tree['latitude'];
				$treeObj->genre_id = $genreB->id;
				$treeObj->save();
			}
		}
		// ** Suppression d'un arbre s'il n'est pas dans la BDD centralisée ******************************* //
		Tree::whereNotIn('centralized_id', $apiTreesId)->delete();
		
		return 'BDD synchronized';
	}
}
