<?php

class GenresController extends BaseController {

	/*
	* Retourne la liste des genres
	*
	* RETURN : format JSON, liste des genres contenant ses arbres
	*/
	public function getGenres()
	{
		$genres = Genre::select(DB::raw('id, name, species, type, centralized_id'))
					->get();
		foreach($genres AS &$genre) {
			$trees = Tree::select(DB::raw('id'))
						->where('genre_id', $genre->id)
						->count();
			$genre['trees'] = $trees;
		}
		return json_encode($genres);
	}
	
	/*
	* Retourne un genre en particulier avec tous ses arbres visibles
	*
	* PARAMS : $id, l'attribut "id" du genre
	* RETURN : format JSON, les attributs d'un genre avec ses arbres visibles 
	*/
	public function getDetails($id)
	{
		$genre = Genre::select(DB::raw('id, name, species, type, centralized_id'))
					->where('id', $id)
					->first();
		$trees = Tree::select(DB::raw('id, name, area, height, trunk_diameter, crown_diameter, longitude, latitude, centralized_id'))
					->where('genre_id', $id)
					->where('visible', 1)
					->get();
		$genre['trees'] = $trees;
		return json_encode($genre);
	}
	
	/*
	* Retourne un genre en particulier avec tous ses arbres, visibles ou non
	*
	* PARAMS : $id, l'attribut "id" du genre
	* RETURN : format JSON, les attributs d'un genre avec ses arbres visibles ou non
	*/
	public function getAllDetails($id)
	{
		$genre = Genre::select(DB::raw('id, name, species, type, centralized_id'))
					->where('id', $id)
					->first();
		$trees = Tree::select(DB::raw('id, name, area, height, trunk_diameter, crown_diameter, longitude, latitude, centralized_id'))
					->where('genre_id', $id)
					->get();
		$genre['trees'] = $trees;
		return json_encode($genre);
	}

	/*
	* Permet d'ajouter un commentaire à un arbre
	*
	* PARAMS : $id, l'attribut "id" de l'arbre
	* RETURN : "successfully added"
	*/
	public function addComment($id) {
		Comment::insert(
			array('pseudo' => Input::get('pseudo'), 'message' => Input::get('message'), 'tree_id' => $id, 'date' => date("Y-m-d H:i:s"), 'visible' => 1)
		);
		return "successfully added";
	}
	
	/*
	* Permet d'ajouter une photo à un arbre
	*
	* PARAMS : $id, l'attribut "id" de l'arbre
	* RETURN : "successfully added"
	*/
	public function addPicture($id) {
		Gallery::insert(
			array('picture' => Input::get('picture'), 'tree_id' => $id, 'date' => date("Y-m-d H:i:s"), 'visible' => 1)
		);
		return "successfully added";
	}

}
