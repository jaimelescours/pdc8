<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameApiUrl extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('trees', function($table) {
			$table->renameColumn('api_url', 'centralized_id');
		});
		Schema::table('genres', function($table) {
			$table->renameColumn('api_url', 'centralized_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('trees', function($table) {
			$table->renameColumn('centralized_id', 'api_url');
		});
		Schema::table('trees', function($table) {
			$table->renameColumn('centralized_id', 'api_url');
		});
	}

}
