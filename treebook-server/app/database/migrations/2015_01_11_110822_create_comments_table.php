<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('comments', function($table)
		{
			$table->increments('id');
			$table->integer('tree_id')->length(10)->unsigned();
			$table->string('pseudo');
			$table->string('message');
			$table->dateTime('date');
			$table->integer('visible');
		});
		
		Schema::table('comments', function($table) {
			$table->foreign('tree_id')
				  ->references('id')->on('trees')
				  ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('comments', function($table) {
			$table->dropForeign('comments_tree_id_foreign');
		});
		Schema::drop('comments');
	}

}
