<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGenresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('genres', function($table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('species');
			$table->string('type');
			$table->string('api_url');
		});
		
		Schema::table('trees', function($table) {
			$table->foreign('genre_id')
				  ->references('id')->on('genres')
				  ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('trees', function($table) {
			$table->dropForeign('trees_genre_id_foreign');
		});
		Schema::drop('genres');
	}

}
