<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('galleries', function($table)
		{
			$table->increments('id');
			$table->integer('tree_id')->length(10)->unsigned();
			$table->string('picture');
			$table->dateTime('date');
			$table->integer('visible');
		});
		
		Schema::table('galleries', function($table) {
			$table->foreign('tree_id')
				  ->references('id')->on('trees')
				  ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('galleries', function($table) {
			$table->dropForeign('galleries_tree_id_foreign');
		});
		Schema::drop('galleries');
	}

}
