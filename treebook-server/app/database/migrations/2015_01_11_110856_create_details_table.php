<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('details', function($table)
		{
			$table->increments('id');
			$table->string('description');
			$table->double('rating');
		});
		
		Schema::table('trees', function($table) {
			$table->foreign('detail_id')
				  ->references('id')->on('details')
				  ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('trees', function($table) {
			$table->dropForeign('details_detail_id_foreign');
		});
		Schema::drop('details');
	}

}
