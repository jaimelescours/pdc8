<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTreesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('trees', function($table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('area');
			$table->integer('genre_id')->length(10)->unsigned();
			$table->integer('detail_id')->length(10)->unsigned();
			$table->double('height');
			$table->double('trunk_diameter');
			$table->double('crown_diameter');
			$table->double('longitude')->nullable();
			$table->double('latitude')->nullable();
			$table->integer('visible');
			$table->string('api_url');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('trees');
	}

}
