<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContactMessage extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contacts', function($table) {
			$table->dropColumn('message');
		});		
		Schema::table('contacts', function($table) {
			$table->text('message');
		});		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contacts', function($table) {
			$table->dropColumn('message');
		});		
		Schema::table('contacts', function($table) {
			$table->string('message');
		});			
	}

}
