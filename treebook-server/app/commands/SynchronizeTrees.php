<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SynchronizeTrees extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'synchronizeTrees';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'This command synchronize all trees with centralized database';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->info(' Start synchronization :');
		$this->info(' ... in progress');
		$controller = new SynchronizeController;
		$controller->syncTrees();
		$this->info(' End !');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 
	protected function getArguments()
	{
		return array(
			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	} */

}
