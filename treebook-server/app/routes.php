<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Ici sont enregistrées toutes les routes de l'application
|
*/

Route::get('/', function()
{
	return View::make('hello on treebook API');
});

/*
|--------------------------------------------------------------------------
| Synchronize Routes : SynchronizeController
|-------------------------------------------------------------------------- 
*/
/*
* Permet de synchroniser les arbres avec la BDD centralisée
*
* URL : /synchronizeTrees
* Controller : SynchronizeController
* Method : syncTrees
*/
Route::get('synchronizeTrees', 'SynchronizeController@syncTrees');

/*
|--------------------------------------------------------------------------
| Trees Routes : TreesController
|-------------------------------------------------------------------------- 
*/
/*
* Retourne l'arbre du moment
*
* URL : /today
* Controller : TreesController
* Method : getTodayTree
*/
Route::get('today', 'TreesController@getTodayTree');

/*
* Retourne les arbres avec l'attribut "visible" à "1"
*
* URL : /trees
* Controller : TreesController
* Method : getTrees
*/
Route::get('trees', 'TreesController@getTrees');

/*
* Retourne tous les arbres (peu importe l'attribut "visible")
*
* URL : /trees/all
* Controller : TreesController
* Method : getAllTrees
*/
Route::get('trees/all', 'TreesController@getAllTrees');

/*
* Retourne tous les arbres remarquables avec l'attribut "visible" à "1"
*
* URL : /trees/remarkable
* Controller : TreesController
* Method : getTreesRemarkable
*/
Route::get('trees/remarkable', 'TreesController@getTreesRemarkable');

/*
* Retourne tous les arbres remarquables (peu importe l'attribut "visible")
*
* URL : /trees/remarkable/all
* Controller : TreesController
* Method : getAllTreesRemarkable
*/
Route::get('trees/remarkable/all', 'TreesController@getAllTreesRemarkable');

/*
* Retourne toutes les informations d'un arbre (avec le détails, les commentaires visibles, les photos visibles)
*
* URL : /trees/{id}
* Controller : TreesController
* Method : getDetails
*/
Route::get('trees/{id}', 'TreesController@getDetails');

/*
* Retourne toutes les informations d'un arbre (avec le détails, les commentaires visibles ou non, les photos visibles ou non)
*
* URL : /trees/{id}/all
* Controller : TreesController
* Method : getAllDetails
*/
Route::get('trees/{id}/all', 'TreesController@getAllDetails');

/*
* Permet d'ajouter un commentaire à un arbre identifié par {id}
*
* URL : /trees/{id}/comments
* Controller : TreesController
* Method : POST addComment
*/
Route::post('trees/{id}/comments', 'TreesController@addComment');

/*
* Permet d'ajouter une note à un arbre identifié par {id}
*
* URL : /trees/{id}/vote
* Controller : TreesController
* Method : POST addVote
*/
Route::post('trees/{id}/vote', 'TreesController@addVote');

/*
* Permet d'ajouter une image à un arbre identifié par {id}
*
* URL : /trees/{id}/galleries
* Controller : TreesController
* Method : POST addPicture
*/
Route::post('trees/{id}/galleries', 'TreesController@addPicture');

/*
* Permet de mettre à jour les détails d'un arbre (description, ...)
*
* URL : /trees/{id}/details
* Controller : TreesController
* Method : PUT editDetails
*/
Route::put('trees/{id}/details', 'TreesController@editDetails');

/*
* Permet de changer le status visible d'un arbre
*
* URL : /galleries/{id}/visible
* Controller : TreesController
* Method : PUT changeVisible
*/
Route::put('trees/{id}/visible', 'TreesController@changeVisible');

/*
* Permet de changer le status remarquable d'un arbre
*
* URL : /galleries/{id}/remarkable
* Controller : TreesController
* Method : PUT changeRemarkable
*/
Route::put('trees/{id}/remarkable', 'TreesController@changeRemarkable');

/*
|--------------------------------------------------------------------------
| Genre Routes : GenresController
|-------------------------------------------------------------------------- 
*/
/*
* Retourne tous les genres
*
* URL : /genres
* Controller : GenresController
* Method : getGenres
*/
Route::get('genres', 'GenresController@getGenres');

/*
* Retourne les informations spécifiques à un genre avec tous les arbres qu'il contient
*
* URL : /genres/{id}
* Controller : GenresController
* Method : getDetails
*/
Route::get('genres/{id}', 'GenresController@getDetails');

/*
* Retourne les informations spécifiques à un genre avec tous les arbres visibles qu'il contient
*
* URL : /genres/{id}/all
* Controller : GenresController
* Method : getAllDetails
*/
Route::get('genres/{id}/all', 'GenresController@getAllDetails');

/*
|--------------------------------------------------------------------------
| Gallery Routes : GalleriesController
|-------------------------------------------------------------------------- 
*/
/*
* Met à jour le nom d'une image en BDD identifiée par son {id}
*
* URL : /galleries/{id}
* Controller : GalleriesController
* Method : PUT edit
*/
Route::put('galleries/{id}', 'GalleriesController@edit');

/*
* Supprime une image identifiée par son {id}
*
* URL : /galleries/{id}
* Controller : GalleriesController
* Method : DELETE delete
*/
Route::delete('galleries/{id}', 'GalleriesController@delete');

/*
* Permet de changer le status d'une photo
*
* URL : /galleries/{id}/visible
* Controller : GalleriesController
* Method : PUT changeVisible
*/
Route::put('galleries/{id}/visible', 'GalleriesController@changeVisible');

/*
|--------------------------------------------------------------------------
| Comment Routes : CommentsController
|-------------------------------------------------------------------------- 
*/
/*
* Met à jour un commentaire identifiée par son {id}
*
* URL : /comments/{id}
* Controller : CommentsController
* Method : PUT edit
*/
Route::put('comments/{id}', 'CommentsController@edit');

/*
* supprime un commentaire identifiée par son {id}
*
* URL : /comments/{id}
* Controller : CommentsController
* Method : DELETE delete
*/
Route::delete('comments/{id}', 'CommentsController@delete');

/*
* Permet de changer le status d'un commentaire
*
* URL : /comments/{id}/visible
* Controller : CommentsController
* Method : PUT changeVisible
*/
Route::put('comments/{id}/visible', 'CommentsController@changeVisible');

/*
|--------------------------------------------------------------------------
| Contacts Routes : ContactsController
|-------------------------------------------------------------------------- 
*/
/*
* retourne tous les messages
*
* URL : /contacts
* Controller : ContactsController
* Method : getMessages
*/
Route::get('contacts', 'ContactsController@getMessages');

/*
* Retourne le nombre de messages non lu
*
* URL : /contacts/nbUnread
* Controller : ContactsController
* Method : getNbUnread
*/
Route::get('contacts/nbUnread', 'ContactsController@getNbUnread');

/*
* Permet d'ajouter un message de contact
*
* URL : /contacts
* Controller : ContactsController
* Method : POST addMessage
*/
Route::post('contacts', 'ContactsController@addMessage');

/*
* Permet de modifier un message identifié par son {id}
*
* URL : /contacts/{id}
* Controller : ContactsController
* Method : PUT changeRead
*/
Route::put('contacts/{id}', 'ContactsController@changeRead');

/*
* Permet de supprimer un message identifié par son {id}
*
* URL : /contacts/{id}
* Controller : ContactsController
* Method : DELETE delete
*/
Route::delete('contacts/{id}', 'ContactsController@delete');

