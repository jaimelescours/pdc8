<?php

/**
 * Class Gallery : contient la liste des images d'un arbre
 *
 * @$table : défini la table MySQL liée au modèle Gallery
 * @$timestamps = false : disable timestamp when update, ...
 */
class Gallery extends Eloquent {
	// ** Attributs *************************************** //
	protected $table = 'galleries';
	public $timestamps = false;
}