<?php

/**
 * Class Genre : ce sont les genres/espèce d'un arbre
 *
 * @$table : défini la table MySQL liée au modèle Genre
 * @$timestamps = false : disable timestamp when update, ...
 * @fillable : tableau qui contient les attributs sur lesquels on va faire des "REPLACE INTO" ou "UPDATE"
 */
class Genre extends Eloquent {
	// ** Attributs *************************************** //
	protected $table = 'genres';
	protected $fillable = array('centralized_id');
	public $timestamps = false;
}