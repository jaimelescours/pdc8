<?php

/**
 * Class Comment : contient les commentaires d'un arbre
 *
 * @$table : défini la table MySQL liée au modèle Comment
 * @$timestamps = false : disable timestamp when update, ...
 */
class Comment extends Eloquent {
	// ** Attributs *************************************** //
	protected $table = 'comments';
	public $timestamps = false;
}