<?php

/**
 * Class Tree : correspond a un arbre (synchronisé avec la BDD centralisée)
 *
 * @$table : défini la table MySQL liée au modèle Tree
 * @$timestamps = false : disable timestamp when update, ...
 * @fillable : tableau qui contient les attributs sur lesquels on va faire des "REPLACE INTO" ou "UPDATE"
 */
class Tree extends Eloquent {

	protected $table = 'trees';
	protected $fillable = array('centralized_id');
	public $timestamps = false;
}