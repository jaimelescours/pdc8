<?php

/**
 * Class Contact : contient les messages laissés par les utilisateurs
 *
 * @$table : défini la table MySQL liée au modèle Contact
 * @$timestamps = false : disable timestamp when update, ...
 */
class Contact extends Eloquent {
	// ** Attributs *************************************** //
	protected $table = 'contacts';
	public $timestamps = false;
}