<?php

/**
 * Class Detail : contient tous les détails d'un arbre (infos qui ne sont pas dans la BDD centralisée : "remarkable", "visible", ...)
 *
 * @$table : défini la table MySQL liée au modèle Detail
 * @$timestamps = false : disable timestamp when update, ...
 */
class Detail extends Eloquent {
	// ** Attributs *************************************** //
	protected $table = 'details';
	public $timestamps = false;
}